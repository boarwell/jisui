
package main

import (
	"log"
	"os"
	"path/filepath"
	"sort"

	"github.com/jung-kurt/gofpdf"
)

func main() {
	pdf := gofpdf.New("P", "mm", "A4", "")

	o := gofpdf.ImageOptions{
		ImageType: "",
		ReadDpi:   true,
	}

	var (
		d string
	)

	// 引数2つ以上ある場合はエラー
	// 1つの場合は、そこを対象ディレクトリに
	// ない場合はカレントディレクトリを対象ディレクトリに
	if len(os.Args) > 2 {
		log.Fatalln("need one argument")
	} else if len(os.Args) == 2 {
		d = os.Args[1]
	} else {
		d, _ = os.Getwd()
	}

	f, err := os.Open(d)
	if err != nil {
		log.Fatalln("error: invalid directory name")
	}

	items, _ := f.Readdirnames(0)

	if !sort.IsSorted(sort.StringSlice(items)) {
		sort.Strings(items)
	}

	for _, v := range items {
		if filepath.Ext(v) != ".jpg" && filepath.Ext(v) != ".jpeg" {
			continue
		}

		i := pdf.RegisterImageOptions(v, o)

		s := gofpdf.SizeType{
			Wd: i.Width(),
			Ht: i.Height(),
		}

		pdf.AddPageFormat("P", s)

		pdf.ImageOptions(v, 0, 0, i.Width(), i.Height(), false, o, 0, "")
	}

	err = pdf.OutputFileAndClose(filepath.Join(d, "done.pdf"))
	if err != nil {
		log.Fatalln(err)
	}

}
