# jisui

カレントディレクトリまたは、引数に指定されたディレクトリにあるjpg, jpegファイルをまとめてPDFにします。

画像ファイルは連番になっていることが前提です。

## 使い方

jpgファイルのある場所で`jisui`を実行するだけです。

```sh
$ cd /path/to/jpg-files
$ jisui
```

または、jpgファイルのある場所を指定して実行することもできます。

```sh
$ jisui /path/to/jpg-files
```
